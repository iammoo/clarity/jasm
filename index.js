let e = require('express');
let moment = require('moment');

let port = 3000

let app = e();


app.get('/', (req, res) => {
    res.send('hi');
});

app.get('/ping', (req, res) => {
    res.send('pong');
});


app.listen(port, () => {
    console.log(`Application listening on port ${port}`);
});
